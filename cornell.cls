\ProvidesClass{cornell}
\LoadClass[a4paper]{article}

\usepackage{tcolorbox}
\tcbuselibrary
{
    breakable,% Allows tcolorboxes to break across pages
    hooks,% Allows usage of hooks, like having an overlay only for the first part of a broken box
    skins,% Used to style the boxes with tikz
    xparse% Used to define document environments and commands
}

% Change the margin geometry so that margin notes can be taken in the empty
% space on the left column.
\usepackage{geometry} % Change geometry of pages
\usepackage{marginnote} % Used to create notes in the margin
\renewcommand*{\raggedleftmarginnote}{} % Align notes to the left
\reversemarginpar
\geometry{
    margin=2cm,
    marginparsep=-.35\textwidth,
    marginparwidth=.325\textwidth,
%    showframe
}

\usepackage[parfill]{parskip} % Modify parindent and parskip
\usepackage{enumitem} % Modify itemize spacing
\setitemize
{
    itemsep=0pt,
    parsep=2pt,
}

\usepackage{bookmark} % Add bookmarks to the resulting PDF
\usepackage{hyperref}
\hypersetup
{
    colorlinks   = true, % Colour links instead of ugly boxes
    urlcolor     = blue, % Colour for external hyperlinks
    linkcolor    = blue, % Colour of internal links
    citecolor    = red   % Colour of citations
}

\date{}

\tcbset
{
    colframe=black,
    colupper=black,
    opacitybacktitle=1,
    opacitytext=1,
    fonttitle=\large\bfseries\sffamily,
}

\NewTColorBox[]{titlebox}{ o }
{
    width=\textwidth,
    lowerbox=invisible,
    bookmark*={level=0}{\@title}
}

\renewcommand{\maketitle}
{
    \vspace{-3em}
    \begin{titlebox}
        \Huge{\sffamily{\@title}}
    \end{titlebox}
}

\NewTColorBox[]{extra}{ m }
{
    width=\textwidth,
    title=#1,
    bookmark*={rellevel=1}{#1},
}

\NewTColorBox[]{term}{ m }
{
    width=.325\textwidth,
}

% A note is a tcolorbox in the right column. It may be associated to a
% term which is provided as an optional argument. If a term is provided
% another left-aligned tcolorbox is created with a bookmark. The bookmark
% label may be provided as a separate option.
% It is also possible to give the note itself a title. In this case the
% note tcolorbox is given the provided title and a bookmark for this
% title is created. This bookmark label may also be altered with the last
% option.
% Options:
% o   --> #1 Optional term displayed as a left-aligned tcolorbox
% o   --> #2 Optional bookmark label for the created term
% d<> --> #3 Optional title of this note
% o   --> #4 Optional bookmark label for the titled noted
\NewTColorBox[]{note}{ o o d<> o }
{
    enhanced,
    breakable,
    IfValueT={#1}{bookmark*={rellevel=1}{\IfNoValueTF{#2}{#1}{#2}}},
    IfValueT={#3}{
        bookmark*={rellevel=2}{\IfNoValueTF{#4}{#3}{#4}},
        title=#3,
    },
    enlarge left by=.34\textwidth,
    width=.66\textwidth,
    parbox=false,% restore main text formatting behavior
    overlay unbroken={
        \IfNoValueF{#1} {%
            \node[anchor=north west, outer sep=0pt, inner sep=0pt] at ([xshift=-.34\textwidth]frame.north west) {
                \begin{term}{#1}
                    #1
                \end{term}
            };
        }{}
    },
    overlay first app={
        \IfNoValueF{#1} {%
            \node[anchor=north west, outer sep=0pt, inner sep=0pt] at ([xshift=-.34\textwidth]frame.north west) {
                \begin{term}{#1}
                    #1
                \end{term}
            };
        }{}
    }
}

% A summary is a tcolorbox that spans both columns. By default it floats to
% the bottom of a page as is the norm for true Cornell-styled notes. It is
% possible to not float the summary by using its starred environment
% alternative.
\NewDocumentEnvironment{summary}{ s }
{
    \IfBooleanF{#1} {% float to bottom when there is no star
        \vfill
    }
    \begin{tcolorbox}[
        IfBooleanT={#1} {% become breakable if there is a star
            enhanced,
            breakable
        },
        title=Summary,
        bookmark*={rellevel=1}{Summary},
        parbox=false,% restore main text formatting behavior
    ]
}
{
    \end{tcolorbox}
}
