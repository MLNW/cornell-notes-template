# Cornell-styled Notes Template

The `cornell.cls` file provides a simple document class which can be used to take digital [Cornell-styled](http://lsc.cornell.edu/notes.html) notes.
Resulting documents do not follow 100% the style described in the link above, but they provide a good enough approximation.

## Usage

See `sample.tex` for a simple example of its usage.

## Installation

Creating a document class is a first for me.
Therefore this process could most likely be improved.
I use the class with MiKTeX for which instructions are below.

### MiKTeX

- Open Explorer at `%APPDATA%\MiKTeX`
- Navigate to correct version
- Go to `tex\latex\local`
- If this path does not exist create it
- Copy the `cornell.cls` there
- Open a command prompt and run `texhash`
- Now the class should be usable

## Sources

This class is based on [this answer to a question](https://tex.stackexchange.com/a/276473/233849) on [https://tex.stackexchange.com](https://tex.stackexchange.com).

It has been improved with the help of:
- [Ignasi](https://tex.stackexchange.com/users/1952/ignasi)
- [leandriss](https://tex.stackexchange.com/users/134144/leandriis)
- [Heiko Oberdiek](https://tex.stackexchange.com/users/16967/heiko-oberdiek)

